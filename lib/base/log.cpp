/**
 * Copyright (c) 2024 ~ 2025 KylinSec Co., Ltd.
 * kiran-cc-daemon is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     tangjie02 <tangjie02@kylinos.com.cn>
 */

#include "lib/base/log.h"

namespace Kiran
{
Q_LOGGING_CATEGORY(accounts, "kcd.accounts");
Q_LOGGING_CATEGORY(appearance, "kcd.appearance");
Q_LOGGING_CATEGORY(audio, "kcd.audio");
Q_LOGGING_CATEGORY(clipboard, "kcd.clipboard");
Q_LOGGING_CATEGORY(display, "kcd.display");
Q_LOGGING_CATEGORY(greeter, "kcd.greeter");
Q_LOGGING_CATEGORY(keyboard, "kcd.keyboard");
Q_LOGGING_CATEGORY(mouse, "kcd.mouse");
Q_LOGGING_CATEGORY(touchpad, "kcd.touchpad");
Q_LOGGING_CATEGORY(keybinding, "kcd.keybinding");
Q_LOGGING_CATEGORY(power, "kcd.power");
Q_LOGGING_CATEGORY(systeminfo, "kcd.systeminfo");
Q_LOGGING_CATEGORY(timedate, "kcd.timedate");
Q_LOGGING_CATEGORY(xsettings, "kcd.xsettings");
}  // namespace Kiran
