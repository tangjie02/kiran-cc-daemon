/**
 * Copyright (c) 2024 ~ 2025 KylinSec Co., Ltd.
 * kiran-cc-daemon is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     tangjie02 <tangjie02@kylinos.com.cn>
 */

#pragma once

#include "keys-component.h"

class QGSettings;
class TouchpadProxy;
class QDBusServiceWatcher;

namespace Kiran
{
class KeysTouchpad : public KeysComponent
{
    Q_OBJECT

public:
    KeysTouchpad();
    virtual ~KeysTouchpad(){};

    virtual void init() override;

private:
    void initTouchpadProxy();
    void enableTouchpad(bool enable);

private:
    virtual void triggerShortCut(const QString &name) override;

private:
    TouchpadProxy *m_touchpadProxy;
    QDBusServiceWatcher *m_touchpadServiceWatcher;
    bool m_hasTouchpadDevice;
};

}  // namespace Kiran
