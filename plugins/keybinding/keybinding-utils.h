/**
 * Copyright (c) 2023 ~ 2024 KylinSec Co., Ltd.
 * ks-ssr is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     tangjie02 <tangjie02@kylinsec.com.cn>
 */

#pragma once

#include <QString>

namespace Kiran
{
class KeybindingUtils
{
public:
    KeybindingUtils();
    virtual ~KeybindingUtils(){};

    static bool isValidKeySequence(const QString& keyCombGtk);
    // 转换为QKeySequence能识别的字符串，例如<ctrl>k -> ctrl+k
    static QString keyCombGtk2Qt(const QString& keyCombGtk);
    // ctrl+k -> <ctrl>k
    static QString keyCombQt2Gtk(const QString& keyCombQt);
};  // namespace KS

}  // namespace Kiran
