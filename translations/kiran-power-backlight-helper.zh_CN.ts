<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::PowerBacklightHelper</name>
    <message>
        <location filename="../plugins/power/tools/power-backlight-helper.cpp" line="124"/>
        <source>This program can only be used by the root user</source>
        <translation>此程序只能由 root 用户使用</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/power-backlight-helper.cpp" line="131"/>
        <source>This program must only be run through pkexec</source>
        <translation>此程序只能通过 pkexec 运行</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/power-backlight-helper.cpp" line="146"/>
        <source>Could not set the value of the backlight</source>
        <translation>无法设置背光的值</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="48"/>
        <source>Whether the backlight device exists.</source>
        <translation>是否存在背光设备。</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="51"/>
        <source>Get backlight monitor directory.</source>
        <translation>获取背光设备目录。</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="54"/>
        <source>Get the current brightness value.</source>
        <translation>获取当前亮度值。</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="57"/>
        <source>Get the max brightness value.</source>
        <translation>获取最大亮度值。</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="60"/>
        <source>Set the brightness value.</source>
        <translation>设置亮度值。</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="85"/>
        <source>No backlights were found on your system</source>
        <translation>系统未发现背光设备</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="98"/>
        <source>Could not get the value of the backlight</source>
        <translation>获取亮度失败</translation>
    </message>
    <message>
        <location filename="../plugins/power/tools/main.cpp" line="113"/>
        <source>Could not get the maximum value of the backlight</source>
        <translation>获取最大亮度失败</translation>
    </message>
</context>
</TS>
