<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::CustomShortcuts</name>
    <message>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
</context>
<context>
    <name>Kiran::KeybindingManager</name>
    <message>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
</context>
<context>
    <name>Kiran::KeysSound</name>
    <message>
        <source>Sound</source>
        <translation>声音</translation>
    </message>
    <message>
        <source>Volume mute</source>
        <translation>静音</translation>
    </message>
    <message>
        <source>Volume down</source>
        <translation>音量降低</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>音量升高</translation>
    </message>
    <message>
        <source>Microphone mute</source>
        <translation>麦克风静音</translation>
    </message>
    <message>
        <source>Microphone volume down</source>
        <translation>麦克风音量降低</translation>
    </message>
    <message>
        <source>Microphone volume up</source>
        <translation>麦克风音量升高</translation>
    </message>
    <message>
        <source>Launch media player</source>
        <translation>启动媒体播放机</translation>
    </message>
    <message>
        <source>Play (or play/pause)</source>
        <translation>播放(或播放/暂停)</translation>
    </message>
    <message>
        <source>Pause playback</source>
        <translation>暂停回放</translation>
    </message>
    <message>
        <source>Stop playback</source>
        <translation>停止回放</translation>
    </message>
    <message>
        <source>Previous track</source>
        <translation>上一音轨</translation>
    </message>
    <message>
        <source>Next track</source>
        <translation>下一音轨</translation>
    </message>
</context>
<context>
    <name>Kiran::KeysSystem</name>
    <message>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <source>Launch help browser</source>
        <translation>启动帮助浏览器</translation>
    </message>
    <message>
        <source>Launch email client</source>
        <translation>启动电子邮件客户端</translation>
    </message>
    <message>
        <source>Lock screen</source>
        <translation>锁住屏幕</translation>
    </message>
    <message>
        <source>Show desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <source>Show start menu</source>
        <translation>显示开始菜单</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>注销</translation>
    </message>
    <message>
        <source>Shut down</source>
        <translation>关机</translation>
    </message>
    <message>
        <source>Home folder</source>
        <translation>主文件夹</translation>
    </message>
    <message>
        <source>Launch calculator</source>
        <translation>启动计算器</translation>
    </message>
    <message>
        <source>Launch web browser</source>
        <translation>启动网际浏览器</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>查找</translation>
    </message>
    <message>
        <source>Launch settings</source>
        <translation>启动设置</translation>
    </message>
</context>
<context>
    <name>Kiran::KeysTouchpad</name>
    <message>
        <source>Touchpad</source>
        <translation>触摸板</translation>
    </message>
    <message>
        <source>Toggle touchpad</source>
        <translation>切换触摸板</translation>
    </message>
    <message>
        <source>Enable touchpad</source>
        <translation>启用触摸板</translation>
    </message>
    <message>
        <source>Disable touchpad</source>
        <translation>禁用触摸板</translation>
    </message>
</context>
</TS>
