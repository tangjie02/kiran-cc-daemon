<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::TimedateFormat</name>
    <message>
        <location filename="../plugins/timedate/timedate-format.cpp" line="118"/>
        <location filename="../plugins/timedate/timedate-format.cpp" line="119"/>
        <location filename="../plugins/timedate/timedate-format.cpp" line="120"/>
        <source>%b %d %Y</source>
        <translation>%Y年%m月%d日</translation>
    </message>
</context>
</TS>
