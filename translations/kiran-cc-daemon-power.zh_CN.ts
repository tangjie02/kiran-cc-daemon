<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::PowerEventButton</name>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="38"/>
        <source>Power Management</source>
        <translation>电源管理</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="64"/>
        <source>Power Off</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="69"/>
        <source>Suspend</source>
        <translation>挂起</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="74"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="79"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="84"/>
        <source>Increase Screen Brightness</source>
        <translation>增加屏幕亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="89"/>
        <source>Decrease Screen Brightness</source>
        <translation>降低屏幕亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="94"/>
        <source>Increase Keyboard Brightness</source>
        <translation>增加键盘亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="99"/>
        <source>Decrease Keyboard Brightness</source>
        <translation>降低键盘亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/event/power-event-button.cpp" line="104"/>
        <source>Toggle Keyboard Backlight</source>
        <translation>切换键盘亮度开关</translation>
    </message>
</context>
<context>
    <name>Kiran::PowerNotificationManager</name>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="125"/>
        <source>Battery Discharging</source>
        <translation>电池正在放电</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="126"/>
        <source>%1 of battery power remaining (%2%)</source>
        <translation>电池电量剩余 %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="133"/>
        <source>UPS Discharging</source>
        <translation>UPS 正在放电</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="134"/>
        <source>%1 of UPS backup power remaining (%2%)</source>
        <translation>UPS 备用电池电量剩余 %1 (%2%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="153"/>
        <source>Battery Charged</source>
        <translation>电池已充满</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="177"/>
        <source>Battery low</source>
        <translation>电池电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="178"/>
        <source>Approximately &lt;b&gt;%1&lt;/b&gt; remaining (%2%)</source>
        <translation>大约剩余 &lt;b&gt;%1&lt;/b&gt;(%2%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="185"/>
        <source>UPS low</source>
        <translation>UPS 电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="186"/>
        <source>Approximately &lt;b&gt;%1&lt;/b&gt; of remaining UPS backup power (%2%)</source>
        <translation>UPS 备用电源的剩余电量大约可维持 &lt;b&gt;%1&lt;/b&gt;，为总电量的 %2%</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="192"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="260"/>
        <source>Mouse battery low</source>
        <translation>鼠标电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="193"/>
        <source>Wireless mouse is low in power (%1%)</source>
        <translation>无线鼠标电量不足(仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="196"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="265"/>
        <source>Keyboard battery low</source>
        <translation>键盘电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="197"/>
        <source>Wireless keyboard is low in power (%1%)</source>
        <translation>无线键盘电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="200"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="270"/>
        <source>PDA battery low</source>
        <translation>PDA 电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="201"/>
        <source>PDA is low in power (%1%)</source>
        <translation>PDA 电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="204"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="275"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="280"/>
        <source>Cell phone battery low</source>
        <translation>手机电池电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="205"/>
        <source>Cell phone is low in power (%1%)</source>
        <translation>手机电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="208"/>
        <source>Media player battery low</source>
        <translation>媒体播放器电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="209"/>
        <source>Media player is low in power (%1%)</source>
        <translation>媒体播放器电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="212"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="285"/>
        <source>Tablet battery low</source>
        <translation>触摸板电池电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="213"/>
        <source>Tablet is low in power (%1%)</source>
        <translation>触摸板电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="216"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="290"/>
        <source>Attached computer battery low</source>
        <translation>连接的计算机电池电量低</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="217"/>
        <source>Attached computer is low in power (%1%)</source>
        <translation>连接的计算机电量不足 (仅剩 %1%)</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="247"/>
        <source>Battery critically low</source>
        <translation>电池电量将用尽</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="248"/>
        <source>Approximately &lt;b&gt;%1&lt;/b&gt; remaining (%2%). Plug in your AC adapter to avoid losing data.</source>
        <translation>大约剩余 &lt;b&gt;%1&lt;/b&gt;(%2%)。接入您的交流电源以避免丢失数据。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="254"/>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="347"/>
        <source>UPS critically low</source>
        <translation>UPS 电量将用尽</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="255"/>
        <source>Approximately &lt;b&gt;%1&lt;/b&gt; of remaining UPS power (%2%). Restore AC power to your computer to avoid losing data.</source>
        <translation>您的 UPS 剩余电量大约可维持 &lt;b&gt;%1&lt;/b&gt;，为总电量的 %2%。请恢复交流电源以防丢失数据。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="261"/>
        <source>Wireless mouse is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>无线鼠标电池电量严重不足 (仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="266"/>
        <source>Wireless keyboard is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>无线键盘电量严重不足 (仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="271"/>
        <source>PDA is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>PDA 电量严重不足(仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="276"/>
        <source>Cell phone is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>您的手机电量严重不足(仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="281"/>
        <source>Media player is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>媒体播放器电量严重不足(仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="286"/>
        <source>Tablet is very low in power (%1%). This device will soon stop functioning if not charged.</source>
        <translation>触摸板电量严重不足(仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="291"/>
        <source>Attached computer is very low in power (%1%). The device will soon shutdown if not charged.</source>
        <translation>连接的此计算机电量严重不足(仅剩 %1%)。如果不充电的话，此设备将会很快停止工作。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="322"/>
        <source>Laptop battery critically low</source>
        <translation>笔记本电池电量将用尽</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="329"/>
        <source>The battery is below the critical level and this computer is about to suspend.&lt;br&gt;&lt;b&gt;NOTE:&lt;/b&gt; A small amount of power is required to keep your computer in a suspended state.</source>
        <translation>电池的电量将用尽，计算机将开始挂起。&lt;br&gt;&lt;b&gt;注意：&lt;/b&gt;将您的计算机保持在挂起状态也需要一点电量。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="334"/>
        <source>The battery is below the critical level and this computer is about to hibernate.</source>
        <translation>电池的电量将用尽，计算机将开始休眠。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="337"/>
        <source>The battery is below the critical level and this computer is about to shutdown.</source>
        <translation>电池的电量将用尽，计算机将开始关机。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="340"/>
        <source>The battery is below the critical level and this computer will &lt;b&gt;power-off&lt;/b&gt; when the battery becomes completely empty.</source>
        <translation>电池的电量将用尽，电池的电量完全用尽时计算机将会&lt;b&gt;关机&lt;/b&gt;。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="354"/>
        <source>The UPS is below the critical level and this computer is about to suspend.&lt;br&gt;&lt;b&gt;NOTE:&lt;/b&gt; A small amount of power is required to keep your computer in a suspended state.</source>
        <translation>UPS的电量将用尽，计算机将开始挂起。&lt;br&gt;&lt;b&gt;注意：&lt;/b&gt;将您的计算机保持在挂起状态也需要一点电量。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="359"/>
        <source>The UPS is below the critical level and this computer is about to hibernate.</source>
        <translation>UPS 电量将用尽，计算机将开始休眠到硬盘。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="362"/>
        <source>The UPS is below the critical level and this computer is about to shutdown.</source>
        <translation>UPS 电量将用尽，计算机将开始关机。</translation>
    </message>
    <message>
        <location filename="../plugins/power/notification/power-notification-manager.cpp" line="365"/>
        <source>The UPS is below the critical level and this computer will &lt;b&gt;power-off&lt;/b&gt; when the UPS becomes completely empty.</source>
        <translation>UPS 电量将用尽。UPS 电量完全用尽时计算将会&lt;b&gt;关机&lt;/b&gt;。</translation>
    </message>
</context>
<context>
    <name>Kiran::PowerUtils</name>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="25"/>
        <source>Less than 1 minute</source>
        <translation>小于1分钟</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="29"/>
        <location filename="../plugins/power/power-utils.cpp" line="36"/>
        <source>%1 minute</source>
        <translation>%1分钟</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="29"/>
        <location filename="../plugins/power/power-utils.cpp" line="36"/>
        <source>%1 minutes</source>
        <translation>%1分钟</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="37"/>
        <source>%1 hour</source>
        <translation>%1小时</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="37"/>
        <source>%1 hours</source>
        <translation>%1小时</translation>
    </message>
</context>
</TS>
