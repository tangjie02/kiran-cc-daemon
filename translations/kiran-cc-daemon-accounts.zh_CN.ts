<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::AccountsUtil</name>
    <message>
        <location filename="../plugins/accounts/accounts-util.cpp" line="181"/>
        <source> (%1, error code: 0x%2)</source>
        <translation> (%1，错误码：0x%2)</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/accounts-util.cpp" line="185"/>
        <source> (error code: 0x%1)</source>
        <translation> (错误码：0x%1)</translation>
    </message>
</context>
<context>
    <name>Kiran::PasswdProcess</name>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="284"/>
        <source>Password modification timeout.</source>
        <translation>修改密码超时。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="335"/>
        <source>The password contains less than %1 character classes.</source>
        <translation>密码至少包含%1个字符类型。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="337"/>
        <source>The password contains less than %1 digits.</source>
        <translation>密码至少包含%1个数字。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="339"/>
        <source>The password contains less than %1 lowercase letters.</source>
        <translation>密码至少包含%1个小写字母。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="341"/>
        <source>The password contains less than %1 non-alphanumeric characters.</source>
        <translation>密码至少包含%1个非字母数字字符。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="343"/>
        <source>The password contains less than %1 uppercase letters.</source>
        <translation>密码至少包含%1个大写字母。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="345"/>
        <source>The password contains monotonic sequence longer than %1 characters.</source>
        <translation>密码包含超过了%1个字符的单调序列。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="347"/>
        <source>The password contains more than %1 characters of the same class consecutively.</source>
        <translation>密码包含超过%1个同类型的连续字符。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="349"/>
        <source>The password contains more than %1 same characters consecutively.</source>
        <translation>密码包含超过%1个的相同连续字符。</translation>
    </message>
    <message>
        <location filename="../plugins/accounts/passwd-process.cpp" line="351"/>
        <source>The password is shorter than %1 characters.</source>
        <translation>密码少于%1个字符。</translation>
    </message>
</context>
</TS>
