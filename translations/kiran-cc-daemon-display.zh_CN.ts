<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Kiran::DisplayManager</name>
    <message>
        <location filename="../plugins/display/display-manager.cpp" line="239"/>
        <source>The scaling rate can only take effect after logging out and logging in again.</source>
        <translation>缩放率需要注销后重新登录才能生效。</translation>
    </message>
</context>
</TS>
